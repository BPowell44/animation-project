﻿using UnityEngine;
using System.Collections;

public class TeleportPlayerLevel4 : MonoBehaviour {

	Canvas levelComplete;
	
	void Start ()
	{
		levelComplete = GameObject.Find ("LevelComplete").GetComponent <Canvas> ();
		levelComplete.enabled = false;
	}

	void OnTriggerEnter(Collider target) 
	{
		if (target.tag == "Player")
			StartCoroutine(LoadLevel("Level5", 1.9f));
	}
	
	IEnumerator LoadLevel(string level, float waitTime)
	{
		levelComplete.enabled = true;
		audio.Play();
		yield return new WaitForSeconds(waitTime);
		Application.LoadLevel(level);
	}
}