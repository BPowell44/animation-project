﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : MonoBehaviour {
	

	public void Quit () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}

	public void StartGame ()
	{
	
		Application.LoadLevel("Level1");

	}

	public void StartTutorial ()
	{
		Application.LoadLevel ("Tutorial");
	}
}
