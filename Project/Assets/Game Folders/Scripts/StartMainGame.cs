﻿using UnityEngine;
using System.Collections;

public class StartMainGame : MonoBehaviour {

	public void StartGame () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.LoadLevel("Level1");
		#endif

	}
}
