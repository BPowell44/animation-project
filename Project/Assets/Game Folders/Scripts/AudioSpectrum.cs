﻿using UnityEngine;
using System.Collections;

public class AudioSpectrum : MonoBehaviour {
	
	public GameObject cube1;
	public GameObject cube2;
	public GameObject cube3;
	public GameObject cube4;
	public GameObject cube5;
	public GameObject cube6;
	public GameObject cube7;
	public GameObject cube8;
	public GameObject cube9;
	public GameObject cube10;

	public float juice = 20f;
	
	public float[] spec;
	
	public float specMag01;
	public float specMag02;
	public float specMag03;
	public float specMag04;
	public float specMag05;
	public float specMag06;
	public float specMag07;
	public float specMag08;
	public float specMag09;
	public float specMag10;


	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//spec = AudioListener.GetSpectrumData(64,0,FFTWindow.Hamming); // this works on audio source
		spec = AudioListener.GetOutputData (64,0);  // this gives much  better values.
		
		specMag01 = spec[2] + spec[6];
		specMag02 = spec[8] + spec[12];
		specMag03 = spec[14] + spec[18];
		specMag04 = spec[20] + spec[24];
		specMag05 = spec[26] + spec[31];
		specMag06 = spec[32] + spec[37];
		specMag07 = spec[38] + spec[43];
		specMag08 = spec[44] + spec[49];
		specMag09 = spec[50] + spec[57];
		specMag10 = spec[58] + spec[62];
	
		
		cube1.gameObject.transform.localScale = new Vector3(1f,specMag01*juice,1f);
		cube2.gameObject.transform.localScale = new Vector3(1f,specMag02*juice,1f);
		cube3.gameObject.transform.localScale = new Vector3(1f,specMag03*juice,1f);
		cube4.gameObject.transform.localScale = new Vector3(1f,specMag04*juice,1f);
		cube5.gameObject.transform.localScale = new Vector3(1f,specMag05*juice,1f);
		cube6.gameObject.transform.localScale = new Vector3(1f,specMag06*juice,1f);
		cube7.gameObject.transform.localScale = new Vector3(1f,specMag07*juice,1f);
		cube8.gameObject.transform.localScale = new Vector3(1f,specMag08*juice,1f);
		cube9.gameObject.transform.localScale = new Vector3(1f,specMag09*juice,1f);
		cube10.gameObject.transform.localScale = new Vector3(1f,specMag10*juice,1f);

	}
}