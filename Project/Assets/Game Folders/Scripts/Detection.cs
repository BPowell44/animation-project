﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour
{
	private GameObject player; // Reference to the player
	Canvas gameOver; // Reference the Canvas

	void Awake ()
	{
		// Setting up the references.
		player = GameObject.FindGameObjectWithTag(Tags.player);
		gameOver = GameObject.Find ("GameOver").GetComponent <Canvas> ();
		gameOver.enabled = false;
	}



	void OnTriggerStay (Collider other)
	{
		// If colliding with the gameobject player then
		if(other.gameObject == player)
		{
			// raycast from the camera towards the player
			Vector3 relPlayerPos = player.transform.position - transform.position;
			RaycastHit hit;

			if(Physics.Raycast(transform.position, relPlayerPos, out hit))
				// If the raycast hits the player
				if(hit.rigidbody.gameObject == player)
					// enable GUI, pause game and run Pause();
					gameOver.enabled = true;
					Time.timeScale = 0.0f;
					Pause();
		}


	}

	void Pause()
	{			// waiting for input
				if (Input.GetButton ("Space")) {
						// restart the time scale
						Time.timeScale = 1.0f;
						// load level
						Application.LoadLevel ("Level1");
				}
		}

}
