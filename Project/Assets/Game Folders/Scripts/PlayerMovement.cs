using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	public float turnSmoothing = 15f;	// A smoothing value for turning the player.
	public float speedDampTime = 0.1f;	// The damping for the speed parameter

	private Animator anim;	// Reference to the animator component.
	private HashIDs hash;	// Reference to the HashIDs.

	private float standHeight = 2f;
	private float crouchHeight = 0.8f;
	CapsuleCollider playerCollider;	
	
	void Awake ()
	{
		// Setting up the references.
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
	}

	void Start ()
	{
				playerCollider = GetComponent <CapsuleCollider> ();
		}
		
	void FixedUpdate ()
	{
		// Cache the inputs.
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		bool sneak = Input.GetButton("Sneak");
		
		Movement(h, v, sneak);
	}

	void Update ()
	{
		heightCollider ();
	}

	void Movement (float horizontal, float vertical, bool sneaking)
	{
		// Set the sneaking parameter to the sneak input.
		anim.SetBool(hash.sneakingBool, sneaking);
		
		// If there is some axis input...
		if(horizontal != 0f || vertical != 0f)
		{
			// Set the players rotation and set the speed parameter to 5.5f.
			Rotating(horizontal, vertical);
			anim.SetFloat(hash.speedFloat, 5.5f, speedDampTime, Time.deltaTime);
		}
		else
			// Otherwise set the speed parameter to 0.
			anim.SetFloat(hash.speedFloat, 0);
	}
		
	void Rotating (float horizontal, float vertical)
	{
		// Create a new vector of the horizontal and vertical inputs.
		Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
		
		// Create a rotation based on this new vector assuming that up is the global y axis.
		Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
		
		// Create a rotation that is an increment closer to the target rotation from the player's rotation.
		Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);

		// Change the players rotation to this new rotation.
		rigidbody.MoveRotation(newRotation);
	}

	void heightCollider ()
	{
		if (Input.GetButton("Sneak")) 
		{
			playerCollider.height = crouchHeight;
			playerCollider.center = new Vector3 (0f, 0.7f, 0f);

		} 

		else
		{
			playerCollider.height = standHeight;
			playerCollider.center = new Vector3 (0f, 1f, 0f);
		}

	}
}


